#include <iostream>
#include <cmath>
#include <string>
#include <stack>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Other includes
#include "Shader.h"
#include "LSys.h"
#include "Camera.h"
#include "Model.h"


// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void do_keys();




// Window dimensions
const GLuint WIDTH = 1920, HEIGHT = 1080;

// Camera
Camera camera(glm::vec3(0.0f, 15.0f, 40.0f));
GLfloat lastX = WIDTH / 2.0,
        lastY = HEIGHT / 2.0;

bool keys[1024];

// Deltatime
GLfloat deltaTime = 0.0f;	// Time between current frame and last frame
GLfloat lastFrame = 0.0f;  	// Time of last frame

//===================================================================================================================================================




glm::vec4 groundColor(0.0f, 0.0f, 0.0f, 1.0f);

glm::vec4 backgroundColor(0.0f, 0.0f, 0.0f, 1.0f);

//GLfloat rColorMod = 0.08f;
//GLfloat gColorMod = 0.08f;
//GLfloat bColorMod = 0.0f;



GLfloat rotate = glm::radians(0.0);
GLfloat rotationDelta =glm::radians(0.0);

bool dispWireFrame = false;
bool dispPoints = false;



//===================================================================================================================================================

Lights *lights_ptr;

Lights *secondLight_ptr;
LSys *l_system;


int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);


    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glewExperimental = GL_TRUE;
    glewInit();

    glViewport(0, 0, WIDTH, HEIGHT);

    glEnable(GL_DEPTH_TEST);


    // Lsystem needs projection matric in init() thus we need to set the projection matrix first
    camera.setProjection(glm::perspective(glm::radians(camera.Zoom), (GLfloat)WIDTH/(GLfloat)HEIGHT, 0.1f, 50000.0f));

    // Setup all
    Lights lights= Lights(&camera);
    glm::vec3 pos(60.0f,70.0f,2.0f);
    glm::vec3 amb(0.1f,0.1f,0.1f);
    glm::vec3 diff(0.4f,1.0f,0.4f);
    glm::vec3 spec(0.0f, 0.0f, 0.0f);

    lights.addLight(pos,amb,diff,spec);
    lights_ptr = &lights;

    // Second light
//    pos= glm::vec3(160.0f,170.0f,52.0f);
//     amb=glm::vec3(0.0f,0.0f,0.0f);
//    lights.addLight(pos,amb,amb,amb);



    // init Lystem
    LSys lsys("FG+-[]", "F",&lights,&camera);
    lsys.addRule('F', "FF+[+F-F-F]-[-F+F+F]*[*F/F/F]/[/F*F*F]");
    l_system = &lsys;



    Model m ("Ground.obj");
    Shader groundShader("vModelLoading.glsl","fModelLoading.glsl");


    // Game loop
    while (!glfwWindowShouldClose(window))
    {

        glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // Calculate deltatime of current frame
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lights.deltaTime=deltaTime;
        lastFrame = currentFrame;

        // Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();
        do_keys();

        if(dispWireFrame) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else if(dispPoints) glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
        else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        // Call drawers
        groundShader.Use();
        glm::mat4 ctm = camera.getProjection()*camera.getViewMatrix();
        ctm = glm::scale(ctm,glm::vec3(3.0f,1.0f,3.0f));

        glUniform3fv(glGetUniformLocation(groundShader.Program,"diffInt"),1,glm::value_ptr(lights.getDiffuse(0)));
        glUniformMatrix4fv(glGetUniformLocation(groundShader.Program,"ctm"),1,GL_FALSE,glm::value_ptr(ctm));
        glUniform3fv(glGetUniformLocation(groundShader.Program,"lightPos"),1,glm::value_ptr(lights.getPos(0)));
        m.Draw(groundShader);


        lights.drawLights();
        lsys.draw();



        glfwSwapBuffers(window);
    }


    glfwTerminate();
    return 0;
}



//===================================================================================================================================================

bool lightToogle=false;

bool shift = false;
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
        l_system->extend();
    if (key == GLFW_KEY_BACKSPACE && action == GLFW_PRESS)
        l_system->collapseTree();
    if (key == GLFW_KEY_F1 && action == GLFW_PRESS) {
        dispWireFrame = !dispWireFrame;
        dispPoints = false;
    }
    if (key == GLFW_KEY_F2 && action == GLFW_PRESS) {
        dispPoints = !dispPoints;
        dispWireFrame = false;
    }
    if(key == GLFW_KEY_F3 && action == GLFW_PRESS) {
        lights_ptr->switchRotate();
    }
    if (key == GLFW_KEY_F4 && action == GLFW_PRESS) {
        l_system->switchDrawMode();
    }
    if (key == GLFW_KEY_F5 && action == GLFW_PRESS) {
        l_system->changeAngle(glm::radians(5.0f));
    }
    if (key == GLFW_KEY_F6 && action == GLFW_PRESS) {

        if(lightToogle) {
//        lights_ptr->positions[0] = glm::vec3(120.0f,125.0f,90.0f);
            lights_ptr->positions[0] = glm::vec3(180.0f, 110.0f, 180.0f);
            lights_ptr->diffuse[0] = glm::vec3(2.0f, 15.0f, 1.5f);
        }
        else{

            glm::vec3 pos(60.0f,70.0f,2.0f);
            glm::vec3 diff(0.2f,1.0f,0.2f);
            lights_ptr->positions[0] =pos;
            lights_ptr->diffuse[0] = diff;
        }

        lightToogle= !lightToogle;
    }
    if(key == GLFW_KEY_LEFT_SHIFT && action == GLFW_PRESS){
        std::cout << "shift pressed" << std::endl;
        if(shift)
            camera.MovementSpeed/=5;
        else
            camera.MovementSpeed*=5;

        shift = !shift;
    }
    if (key >= 0 && key < 1024)
    {
        if (action == GLFW_PRESS)
            keys[key] = true;
        else if (action == GLFW_RELEASE)
            keys[key] = false;
    }
}

void do_keys()
{
    if (keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

bool firstMouse = true;
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}