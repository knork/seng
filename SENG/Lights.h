//
// Created by Knork on 23.06.2016.
//

#ifndef SENG_LIGHT_H
#define SENG_LIGHT_H
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include "Shader.h"
#include "Camera.h"
#include "Cube.h"

class Lights{
//    std::vector<glm::vec3> positions;
//    std::vector<glm::vec3> ambient;
//    std::vector<glm::vec3> diffuse;
//    std::vector<glm::vec3> specular;
    Shader *lightShader;
    Camera *camera;
    GLuint VBO, VAO;
    GLuint modelLoc,viewLoc,projLoc;
    glm::mat4 rotation;
    GLfloat rotateBy=glm::radians(15.0f);
    bool rotate=false;



    void init(){
        lightShader = new Shader("vShaderLamp.glsl", "fShaderLamp.glsl");


        GLfloat vertices[Cube::size()];
        Cube::getCube(glm::vec3(0.5,0.5,0.5),vertices);

        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);



        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glBindVertexArray(VAO);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
        glEnableVertexAttribArray(1);
        glBindVertexArray(0);

        // Projection Matrix needs to be transmitted only once
        projLoc = glGetUniformLocation(lightShader->Program, "projection");
        modelLoc = glGetUniformLocation(lightShader->Program, "model");
        viewLoc = glGetUniformLocation(lightShader->Program, "view");

    }

public:

    std::vector<glm::vec3> positions;
    std::vector<glm::vec3> ambient;
    std::vector<glm::vec3> diffuse;
    std::vector<glm::vec3> specular;

    GLfloat deltaTime=0.0f;
    Lights(Camera *cam){
        init();
        camera=cam;
    }




    void switchRotate(){
        rotate=!rotate;
    }

    void addLight(glm::vec3 pos, glm::vec3 amb, glm::vec3 diff, glm::vec3 spec){
        positions.push_back(pos);
        ambient.push_back(amb);
        diffuse.push_back(diff);
        specular.push_back(spec);
    }
    glm::vec3 getPos(int i){

        glm::vec3 out = glm::vec3( rotation * glm::vec4(positions[i],1.0f));
        return out;
    }
    glm::vec3 getAmbient(int i){
        return ambient[i];
    }
    glm::vec3 getDiffuse(int i){
        return diffuse[i];
    }
    glm::vec3 getSpecular(int i){
        return specular[i];
    }
    size_t size(){
        return ambient.size();
    }

    void drawLights(){



        lightShader->Use();

        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(camera->getViewMatrix()));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(camera->getProjection()));



        if(rotate){
            rotation = glm::rotate(rotation,rotateBy*deltaTime,glm::vec3(0.0f,1.0f,0.0f));
        }
        glm::mat4 lampModel;
        for(size_t i =0;i < positions.size();i++){
          //  positions[i] =glm::vec3( rotation * glm::vec4(positions[i],1.0f));
        lampModel = glm::translate(lampModel,glm::vec3( rotation * glm::vec4(positions[i],1.0f)) );

        lampModel = glm::scale(lampModel, glm::vec3(2.0f, 2.0f, 2.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(lampModel));

        //  glUniform3f(glGetUniformLocation(lampShader->Program, "lightColor"), lightColor.x, lightColor.y, lightColor.z)
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
            glBindVertexArray(0);
        }

    }
};

#endif //SENG_LIGHT_H
