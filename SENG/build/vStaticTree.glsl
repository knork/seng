#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;

out vec3 FragPos;
out vec3 Normal;
out vec3 Color;


uniform mat4 ctm;


void main() {

    gl_Position = ctm* vec4(position,1.0);
    FragPos = position;
    Normal = normal;
    Color = color;


}
