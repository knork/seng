#version 330 core

in vec2 TexCoords;

out vec4 color;
in vec3 Normal;
in vec3 FragPos;
uniform vec3 lightPos;
uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;

void main()
{

 // NORMALIZE inputs
    vec3 LIGHTDIR = lightPos - FragPos;
    float d = length(LIGHTDIR);
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(LIGHTDIR);




    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = (diff*vec3(0.1,0.7,0.1)).xyz;

    diffuse = 1200* diffuse/( d*d + 0.2*d );


      color = vec4(diffuse,1.0f);
        //
}

