#version 330 core

struct Material {
    vec3 ambient;   //defines Objects color under (here:) ambient light
    vec3 diffuse;
    vec3 specular;
    float shininess;    //scattering / radius of specular light
};

struct Light {
    vec3 position;

    vec3 ambient;   //intensities
    vec3 diffuse;
    vec3 specular;
};
// FragPos WORLD coords
in vec3 FragPos;
// Normal WORLD coords
in vec3 Normal;

in vec3 Color;

out vec4 fColor;

//uniform vec4 uColor;

uniform Material material;
uniform Light light;
uniform vec3 viewPos;
uniform vec2 center;

void main() {


    //Ambient Light
    vec3 ambient = light.ambient * Color;

    // NORMALIZE inputs
    vec3 LIGHTDIR = light.position - FragPos;
    float d = length(LIGHTDIR);
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(LIGHTDIR);
    vec3 viewDir = normalize(viewPos - FragPos);


    // "GLOBAL" DIFFUSE
    vec3 centerNorm = normalize(FragPos-vec3(center,FragPos.z));
    float gDiff = max(dot(centerNorm, lightDir), 0.0);
    vec3 gDiffuse = light.diffuse * (gDiff *Color);

    // DIFFUSE CALC
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * (diff *Color);

    diffuse = 0.1* diffuse + 0.9*gDiffuse;


    //SPECULAR CALC

    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * (spec * material.specular);


    // DISTANCE CALC



      ambient = ambient /(d*d);
    diffuse = diffuse/( d*d + 0.2*d );
    specular = specular/( d*d + 0.3*d );

    float lightStrength = 4000;

    vec3 result = 500*ambient + min(lightStrength*diffuse,vec3(0.3,.9,.2))+specular;
    fColor = vec4(result, 1.0f);

  //  color = vec4(0.0,0.7,0.0,1.0);

    //color = uColor;
}