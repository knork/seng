//
// Created by lucas on 18.06.16.
//

#ifndef L_SYS_TEST2_LSYS_H
#define L_SYS_TEST2_LSYS_H

#include <string>
#include <vector>
#include <sstream>
#include "Camera.h"
#include "Cube.h"
#include "Lights.h"
#include "Shader.h"
#include <glm/gtx/string_cast.hpp>


class LSys {
private:

    // Predefined values

    GLfloat objectMod = 1.0f;
    GLfloat objectHeight = 2.0f;
    GLfloat angle = glm::radians(15.0f);
    GLboolean isStatic=false;

    // Variables used within the object
    GLuint VBO, VAO;
    GLuint staticVBO,staticVAO;
    GLint projLoc, viewLoc, modelLoc;
    Shader *treeShader;
    Shader *staticShader;



    Camera *camera;
    Lights *lights;

    // Format pos,normal,color
    std::vector<glm::vec3> *staticData= nullptr;
    std::stack<glm::mat4> *modelStack=nullptr;
    std::stack<glm::vec4> *colorStack=nullptr;
    // Stores each iteration of the L-system in order collaapse the tree
    std::stack<std::string> treeStack;
    std::string instruction;


    class Rule {
    public:
        char key;
        std::string replacement;

        Rule(char key, std::string replacement) {
            this->key = key;
            this->replacement = replacement;
        }
    };

    std::string alphabet;
    std::string axiom;
    std::vector<Rule *> rules;
    std::string currentString;

public:
    LSys(std::string alphabet, std::string axiom, Lights *l, Camera *cam) {

        modelStack = new std::stack<glm::mat4>();
        colorStack = new std::stack<glm::vec4>();
        this->alphabet = alphabet;
        this->axiom = axiom;
        currentString = axiom;
        treeStack.push(getAxiom());
        this->lights = l;
        camera = cam;
        init();
        instruction = getAxiom();
     //   switchDrawMode();
    }

    ~LSys() {
        // TODO free Rule memory
        delete treeShader;
        delete modelStack;
        delete colorStack;
        glDeleteVertexArrays(1, &VAO);
        glDeleteBuffers(1, &VBO);
    }

    void changeAngle(float by){
        angle+=by;
    }

    void init() {

        treeShader = new Shader("vShaderTree.glsl", "fShaderTree.glsl");

        GLfloat vertices[Cube::size()];
        //  Cube::getCube(glm::vec3(0.1,0.8,0.1),vertices);
        Cube::getCube(glm::vec3(0.1, 1.5, 0.1), vertices);
        // 1 Ast und normalen vektoren

        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glBindVertexArray(VAO);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid *) 0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid *) (3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);

        glBindVertexArray(0);

        // Projection Matrix needs to be transmitted only once
        projLoc = glGetUniformLocation(treeShader->Program, "projection");
        modelLoc = glGetUniformLocation(treeShader->Program, "model");
        viewLoc = glGetUniformLocation(treeShader->Program, "view");


    }

    void draw(){
        if(isStatic){
            staticDraw();
        }else{
            dynDraw();
        }

    }






    std::string getAxiom() {
        return axiom;
    }

    std::string getCurrentString() {
        return currentString;
    }

    void setCurrentString(std::string newString) {
        currentString = newString;
    }

    void addRule(char key, std::string replacement) {
        if (alphabet.find(key) == std::string::npos) throw -1;
        rules.push_back(new Rule(key, replacement));
    }


    void collapseTree() {
        if (treeStack.size() > 1) {
            treeStack.pop();
            setCurrentString(treeStack.top());
            instruction = getCurrentString();
        }
    }

    void extend() {

        std::string nextString;
        std::string tempChar;
        bool found = false;

        for (unsigned int i = 0; i < currentString.size(); i++) {
            found = false;
            for (int n = 0; n < rules.size() && !found; n++) {
                if (currentString.at(i) == rules[n]->key) {
                    nextString.append(rules[n]->replacement);
                    found = true;
                }
            }
            if (!found) {
                tempChar = currentString.at(i);
                nextString.append(tempChar);

            }
        }
        currentString = nextString;
        treeStack.push(getCurrentString());
        instruction = getCurrentString();
        std::cout << instruction << std::endl;
    }

    void switchDrawMode() {
        if(!isStatic){
            isStatic=true;
            instruction= getCurrentString();
            buildStatic();
            compileStaticShader();
            loadStaticData();
        }else{
            isStatic= false;

        }

    }


private:






    void buildStatic() {

        glm::mat4 model;

        model = glm::translate(model, glm::vec3(-1.0f, 0.0, 0.0));

        std::srand(0);

        if(staticData)
            delete staticData;
        staticData = new std::vector<glm::vec3>();

        // get own set of vertices

        GLfloat vertices[Cube::size()];
        Cube::getCube(glm::vec3(0.1, 1.5, 0.1), vertices);

        GLfloat xzCorrection = 0.483f;
        GLfloat yCorrection = 0.20f;

        glm::vec4 color(0.2f, 0.6f, 0.2f,1.0);

        float change = 0.0;
        float changeBy = 0.1;
        std::srand(0);

        glm::vec4 current;

        float r(0.0f);
        glm::vec3 curColor;

        for (int i = 0; i < instruction.length(); i++) {
            switch (instruction.at(i)) {
                case 'F':
                    //Draw Line
                    r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                    curColor= glm::vec3(0.1, 0.4 + r * 0.6, 0.15f);

                    for(size_t i =0; i < 36;i++){
                        size_t cur = i*6;

                        // position Data
                        current = model * glm::vec4(vertices[cur],vertices[cur+1],vertices[cur+2],1.0f);
                        staticData->push_back(glm::vec3(current.x,current.y,current.z));

                        //Normal Data
                        glm::mat4 transpos = glm::transpose(glm::inverse(model));
                        current = transpos * glm::vec4(vertices[cur+3],vertices[cur+4],vertices[cur+5],1.0f);
                        staticData->push_back(glm::vec3(current.x,current.y,current.z));

                        // Color Data^

                        staticData->push_back(curColor);



                    }
                    model = glm::translate(model, glm::vec3(0.0f, objectHeight, 0.0));
                    // transform each point on CPU


                    break;
                case 'G':
                    //Translate
                    model = glm::translate(model, glm::vec3(0.0f, objectHeight, 0.0f));
                    break;
                case '+':
                    //rotate +angle -> Z-Axis
                    model = glm::translate(model, glm::vec3(-xzCorrection, -yCorrection, 0.0f));
                    model = glm::rotate(model, angle, glm::vec3(0.0f, 0.0f, 1.0f));
                    break;
                case '-':
                    //rotate -angle -> Z-Axis
                    model = glm::translate(model, glm::vec3(xzCorrection, -yCorrection, 0.0f));
                    model = glm::rotate(model, -angle, glm::vec3(0.0f, 0.0f, 1.0f));
                    break;
                case '*':
                    //Rotate +angle -> X-Axsis
                    model = glm::translate(model, glm::vec3(0.0f, -yCorrection, xzCorrection));
                    model = glm::rotate(model, angle, glm::vec3(1.0f, 0.0f, 0.0f));
                    break;
                case '/':
                    //Rotate -angle -> X-Axis
                    model = glm::translate(model, glm::vec3(0.0f, -yCorrection, -xzCorrection));
                    model = glm::rotate(model, -angle, glm::vec3(1.0f, 0.0f, 0.0f));
                    break;
                case '[':
                    //pushStack
                    modelStack->push(model);
                    colorStack->push(color);
                    break;
                case ']':
                    //popStack
                    model = modelStack->top();
                    modelStack->pop();
                    color = colorStack->top();
                    colorStack->pop();
                    break;
                default:
                    std::cout << "No match..." << std::endl;
                    break;
            }
        }


    }



    void drawString(glm::mat4 model) {

        GLfloat xzCorrection = 0.483f;
        GLfloat yCorrection = 0.20f;

        glm::vec4 color(0.2f, 0.8f, 0.2f, 1.0f);

        float change = 0.0;
        float changeBy = 0.1;
        std::srand(0);
        for (int i = 0; i < instruction.length(); i++) {
            switch (instruction.at(i)) {
                case 'F':
                    //Draw Line
                    glUniformMatrix4fv(glGetUniformLocation(treeShader->Program, "model"), 1, GL_FALSE,
                                       glm::value_ptr(model));
                    glUniform3f(glGetUniformLocation(treeShader->Program, "material.ambient"), color.r, color.g,
                                color.b);
                    glUniform3f(glGetUniformLocation(treeShader->Program, "material.diffuse"), color.r, color.g,
                                color.b);
                    glUniform3f(glGetUniformLocation(treeShader->Program, "material.specular"), 0.1f, 0.1f, 0.f);

                    glDrawArrays(GL_TRIANGLES, 0, 36);
                    model = glm::translate(model, glm::vec3(0.0f, objectHeight, 0.0));


                    if (change < 1.0) {
                        // brown
                        color = glm::vec4(0.15, 0.25, 0.2, 1.0);
                    } else {
                        // green
                        float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                        color = glm::vec4(0.1, 0.4 + r * 0.6, 0.15, 1.0);

                    }
                    change += changeBy;
//                color.r += rColorMod;
//                color.g += gColorMod;
//                color.b += bColorMod;
                    break;
                case 'G':
                    //Translate
                    model = glm::translate(model, glm::vec3(0.0f, objectHeight, 0.0f));
                    break;
                case '+':
                    //rotate +angle -> Z-Axis
                    model = glm::translate(model, glm::vec3(-xzCorrection, -yCorrection, 0.0f));
                    model = glm::rotate(model, angle, glm::vec3(0.0f, 0.0f, 1.0f));
                    break;
                case '-':
                    //rotate -angle -> Z-Axis
                    model = glm::translate(model, glm::vec3(xzCorrection, -yCorrection, 0.0f));
                    model = glm::rotate(model, -angle, glm::vec3(0.0f, 0.0f, 1.0f));
                    break;
                case '*':
                    //Rotate +angle -> X-Axsis
                    model = glm::translate(model, glm::vec3(0.0f, -yCorrection, xzCorrection));
                    model = glm::rotate(model, angle, glm::vec3(1.0f, 0.0f, 0.0f));
                    break;
                case '/':
                    //Rotate -angle -> X-Axis
                    model = glm::translate(model, glm::vec3(0.0f, -yCorrection, -xzCorrection));
                    model = glm::rotate(model, -angle, glm::vec3(1.0f, 0.0f, 0.0f));
                    break;
                case '[':
                    //pushStack
                    modelStack->push(model);
                    colorStack->push(color);
                    break;
                case ']':
                    //popStack
                    model = modelStack->top();
                    modelStack->pop();
                    color = colorStack->top();
                    colorStack->pop();
                    break;
                default:
                    std::cout << "No match..." << std::endl;
                    break;
            }
        }


    }

    void compileStaticShader(){
        staticShader = new Shader("vStaticTree.glsl","fStaticTree.glsl");
    }



    void loadStaticData(){

        // free old resources!
        glDeleteVertexArrays(1, &staticVAO);
        glDeleteBuffers(1, &staticVBO);

        // Load New Data
        glGenVertexArrays(1, &staticVAO);
        glGenBuffers(1, &staticVBO);




        glBindBuffer(GL_ARRAY_BUFFER, staticVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*(staticData->size()), &(staticData->operator[](0)), GL_STATIC_DRAW);

        glBindVertexArray(staticVAO);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid *) 0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid *) (3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid *) (6 * sizeof(GLfloat)));
        glEnableVertexAttribArray(2);

        glBindVertexArray(0);


    }

    void staticDraw(){
        //Use corresponding Shader when setting uniforms/drawing objects
        staticShader->Use();
        glm::vec3 lightPos = lights->getPos(0);
        glUniform3f(glGetUniformLocation(staticShader->Program, "light.position"), lightPos.x, lightPos.y, lightPos.z);

        //Light properties

        glUniform3fv(glGetUniformLocation(staticShader->Program, "light.ambient"),1,glm::value_ptr(lights->getAmbient(0)));
        glUniform3fv(glGetUniformLocation(staticShader->Program, "light.diffuse"),1,glm::value_ptr(lights->getDiffuse(0)));
        glUniform3fv(glGetUniformLocation(staticShader->Program, "light.specular"),1,glm::value_ptr(lights->getSpecular(0)));

        //Material properties
        glUniform3f(glGetUniformLocation(staticShader->Program, "material.ambient"), 0.01f, 0.01f, 0.02f); // not being USED!
        glUniform3f(glGetUniformLocation(staticShader->Program, "material.diffuse"), 0.6f, 1.0f, 0.5f); // not being used!
        glUniform3f(glGetUniformLocation(staticShader->Program, "material.specular"), 1.20, 1.0f, 1.0f);
        glUniform1f(glGetUniformLocation(staticShader->Program, "material.shininess"), 2.0f);
        glUniform3fv(glGetUniformLocation(staticShader->Program, "viewPos"), 1, glm::value_ptr(camera->Position));
        glUniform2f(glGetUniformLocation(staticShader->Program, "center"), 0.0f, 0.0f);


        glm::mat4 ctm = camera->getProjection() * camera->getViewMatrix();

        glUniformMatrix4fv(glGetUniformLocation(staticShader->Program,"ctm"),1,GL_FALSE,glm::value_ptr(ctm));


        glBindVertexArray(staticVAO);
        glDrawArrays(GL_TRIANGLES,0,staticData->size()/3);
        glBindVertexArray(0);
    }

    void dynDraw() {

        //Use corresponding Shader when setting uniforms/drawing objects
        treeShader->Use();
        GLint lightPosLoc = glGetUniformLocation(treeShader->Program, "light.position");
        glm::vec3 lightPos = lights->getPos(0);
        glUniform3f(lightPosLoc, lightPos.x, lightPos.y, lightPos.z);

        //Light properties

        glUniform3fv(glGetUniformLocation(treeShader->Program, "light.ambient"),1,glm::value_ptr(lights->getAmbient(0)));
        glUniform3fv(glGetUniformLocation(treeShader->Program, "light.diffuse"),1,glm::value_ptr(lights->getDiffuse(0)));
        glUniform3fv(glGetUniformLocation(treeShader->Program, "light.specular"),1,glm::value_ptr(lights->getSpecular(0)));

        //Material properties
        glUniform3f(glGetUniformLocation(treeShader->Program, "material.ambient"), 0.1f, 0.1f, 0.2f);
        glUniform3f(glGetUniformLocation(treeShader->Program, "material.diffuse"), 0.6f, 1.0f, 0.5f);
        glUniform3f(glGetUniformLocation(treeShader->Program, "material.specular"), 1.20, 1.0f, 1.0f);
        glUniform1f(glGetUniformLocation(treeShader->Program, "material.shininess"), 2.0f);
        glUniform3fv(glGetUniformLocation(treeShader->Program, "viewPos"), 1, glm::value_ptr(camera->Position));
        glUniform2f(glGetUniformLocation(treeShader->Program, "center"), 0.0f, 0.0f);

        glm::mat4 model(1.0f);
        model = glm::translate(model, glm::vec3(-1.0f, 0.0, 0.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(camera->getViewMatrix()));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(camera->getProjection()));


        glBindVertexArray(VAO);

        drawString(model);
        glBindVertexArray(0);


    }


};




#endif //L_SYS_TEST2_LSYS_H
