//
// Created by Knork on 25.06.2016.
//

#include <gl/glew.h>

#include <GLFW/glfw3.h>
#include "Lights.h"
#include "Camera.h"

GLuint WIDTH = 1920, HEIGHT = 1080;

int main() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow *window = glfwCreateWindow(WIDTH, 1080, "LearnOpenGL", nullptr, nullptr);
    glfwMakeContextCurrent(window);


    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glewExperimental = GL_TRUE;
    glewInit();

    glViewport(0, 0, WIDTH, 1080);

    glEnable(GL_DEPTH_TEST);

    Camera camera;
    camera.setProjection(glm::perspective(camera.Zoom, (GLfloat)WIDTH/(GLfloat)HEIGHT, 0.1f, 500.0f));

    Lights lights(&camera);

    glm::vec3 pos(100.0f,50.0f,100.0f);
    glm::vec3 amb(0.1f,0.1f,0.1f);
    glm::vec3 diff(0.5f,0.7f,1.0f);
    glm::vec3 spec(1.0f, 1.0f, 1.0f);
    lights.addLight(pos,amb,diff,spec);



    while (!glfwWindowShouldClose(window)) {



        glClearColor(0.0,0.0,0.0,1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        lights.drawLights();

        glfwSwapBuffers(window);

    }


};