What a future Config file might look like!
Not implemented atm

/*
ESC 		Close
Enter 		expand Tree
Backspace	collapseTree
F1			Wireframe toggle
F2			Point toggle
F3 			Rotate toggle
F4			Static draw toggle

Base object is always a box

Alphabet can be defined with these:

push			 : push Stack
pop  			 : pop Stack
r(angle,x,y,z)   : rotate by angle(degree) around vec3(x,y,z)
t(x,y,z)		 : translate by vec3(x,y,z)
s(x,y,z)		 : scale by vec3(x,y,z)
c(x,y,z)		 : set color vec3(x,y,z)

*/

# define variables here
$height = 2.0
$width = 1.0
$length = 1.0
$angle = 45.0
$xzCorrection = 0.483
$yCorrection = 0.20

# define alphabet here
[ -> push
] -> pop
G -> t(0.0,$height,0.0)
+ -> t(-xzCorrection, -yCorrection, 0.0) , r($angle,0.0,0.0,1.0)
- -> t(xzCorrection, -yCorrection, 0.0) , r(-$angle,0.0,0.0,1.0)
* -> t(0.0, -yCorrection, xzCorrection) , r($angle,1.0,0.0,0.0)
/ -> t(0.0, -yCorrection, -xzCorrection) , r(-$angle,1.0,0.0,0.0)

# define Rules here
root -> F
rule -> (F, FF+[+F-F-F]-[-F+F+F]*[*F/F/F]/[/F*F*F])